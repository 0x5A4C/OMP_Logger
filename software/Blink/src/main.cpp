#include "Arduino.h"
#include <avr/power.h>
#include "LowPower.h"

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop()
{
  LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);

  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  // delay(500);
}
