#ifndef APP_HPP
#define APP_HPP

#include "system/system.hpp"

namespace app
{
  void init();
  void process();
}

#endif
