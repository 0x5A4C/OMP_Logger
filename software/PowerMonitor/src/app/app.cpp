#include "app.hpp"

#include <Time.h>

void app::init(void)
{
  sys::lcd.begin();
  sys::pwrMon_t::reset();

  sys::lcd.clear();
}

void app::process(void)
{
  //sys::lcd.clear();

  sys::lcd.setCursor(0, 0);
  sys::lcd.setFontSize(FONT_SIZE_MEDIUM);
  if (hour() < 10)
    sys::lcd.print('0');
  sys::lcd.print(hour());
  sys::lcd.print(":");
  if (minute() < 10)
    sys::lcd.print('0');
  sys::lcd.print(minute());
  sys::lcd.print(":");
  if (second() < 10)
    sys::lcd.print('0');
  sys::lcd.print(second());

  sys::lcd.setCursor(0, 2);
  sys::lcd.setFontSize(FONT_SIZE_MEDIUM);
  sys::lcd.print(sys::pwrMon_t::battery_mAh);
  sys::lcd.print(" mAh");

  sys::lcd.setCursor(0, 4);
  sys::lcd.setFontSize(FONT_SIZE_MEDIUM);
  sys::lcd.print(sys::pwrMon_t::miliA);
  sys::lcd.print(" mA");

  digitalWrite(LED_BUILTIN, LOW);
  delay(1000);
}
