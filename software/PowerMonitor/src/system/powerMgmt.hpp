#ifndef POWERMGMT_HPP
#define POWERMGMT_HPP

#include "../platform/platform.hpp"

class PowerMgmt
{
public:
  PowerMgmt(){};

  virtual void setOn()
  {
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
  }
protected:
private:
};

#endif
