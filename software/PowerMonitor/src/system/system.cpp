#include "system.hpp"

#include "Arduino.h"

PowerMgmt sys::powerMgmt;
LCD_SSD1306 sys::lcd;
//Ltc4150<2, 3, 4, 5, 6, 7> powerMonitor;
//sys::pwrMon_t sys::powerMonitor;

void sys::init(void)
{
  pinMode(LED_BUILTIN, OUTPUT);

  //powerMonitor.init();
  pwrMon_t::init();
}

void sys::process(void)
{
  //powerMgmt.setOn();

  // digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  // delay(1000);                       // wait for a second
  // digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  // delay(1000);

  //powerMonitor.process();
  pwrMon_t::process();
}
