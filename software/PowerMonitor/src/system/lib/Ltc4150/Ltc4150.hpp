#ifndef LTC4150_HPP
#define LTC4150_HPP

#include <stdint.h>
#include <Arduino.h>

// void intHdl();

template <uint8_t VIO, uint8_t INT,  uint8_t POL,  uint8_t GND, uint8_t CLR, uint8_t SHDN>
class Ltc4150
{
public:
  static void init();
  static void process();
  static void reset();
  static void intHndl();

  static volatile double miliA;
  static volatile long int time;
  static volatile double battery_mAh;

protected:
private:

  static volatile boolean isrflag;
  static volatile long int lasttime;
  static double ah_quanta; // mAh for each INT
};

template <uint8_t VIO, uint8_t INT,  uint8_t POL,  uint8_t GND, uint8_t CLR, uint8_t SHDN>
static volatile double Ltc4150<VIO, INT, POL, GND, CLR, SHDN>::battery_mAh = 0;

template <uint8_t VIO, uint8_t INT,  uint8_t POL,  uint8_t GND, uint8_t CLR, uint8_t SHDN>
static volatile boolean Ltc4150<VIO, INT, POL, GND, CLR, SHDN>::isrflag = false;

template <uint8_t VIO, uint8_t INT,  uint8_t POL,  uint8_t GND, uint8_t CLR, uint8_t SHDN>
static volatile long int Ltc4150<VIO, INT, POL, GND, CLR, SHDN>::time = 0;

template <uint8_t VIO, uint8_t INT,  uint8_t POL,  uint8_t GND, uint8_t CLR, uint8_t SHDN>
static volatile long int Ltc4150<VIO, INT, POL, GND, CLR, SHDN>::lasttime = 0;

template <uint8_t VIO, uint8_t INT,  uint8_t POL,  uint8_t GND, uint8_t CLR, uint8_t SHDN>
volatile double Ltc4150<VIO, INT, POL, GND, CLR, SHDN>::miliA;

template <uint8_t VIO, uint8_t INT,  uint8_t POL,  uint8_t GND, uint8_t CLR, uint8_t SHDN>
double Ltc4150<VIO, INT, POL, GND, CLR, SHDN>::ah_quanta = 0.17067759; // mAh for each INT

template <uint8_t VIO, uint8_t INT,  uint8_t POL,  uint8_t GND, uint8_t CLR, uint8_t SHDN>
static void Ltc4150<VIO, INT, POL, GND, CLR, SHDN>::init()
{

  // Set up I/O pins:
  pinMode(GND,OUTPUT); // Make this pin LOW for "ground"
  digitalWrite(GND,LOW);

  pinMode(VIO,OUTPUT); // Make this pin HIGH for logic reference
  digitalWrite(VIO,HIGH);

  pinMode(INT,INPUT); // Interrupt input pin (must be D2 or D3)

  pinMode(POL,INPUT); // Polarity input pin

  pinMode(CLR,INPUT); // Unneeded, disabled by setting to input

  pinMode(SHDN,INPUT); // Unneeded, disabled by setting to input

  // Enable active-low interrupts on D3 (INT1) to function myISR().
  // On 328 Arduinos, you may also use D2 (INT0), change '1' to '0'.

  isrflag = false;
  attachInterrupt(1, intHndl, FALLING);
}

template <uint8_t VIO, uint8_t INT,  uint8_t POL,  uint8_t GND, uint8_t CLR, uint8_t SHDN>
static void Ltc4150<VIO, INT, POL, GND, CLR, SHDN>::process()
{
  if (isrflag)
  {
    // Clear the flag (so we only run this once per int)
    isrflag = false;
  }
}

template <uint8_t VIO, uint8_t INT,  uint8_t POL,  uint8_t GND, uint8_t CLR, uint8_t SHDN>
static void Ltc4150<VIO, INT, POL, GND, CLR, SHDN>::reset()
{
  miliA = 0;
  time = 0;
  battery_mAh = 0;
}

template <uint8_t VIO, uint8_t INT,  uint8_t POL,  uint8_t GND, uint8_t CLR, uint8_t SHDN>
static void Ltc4150<VIO, INT, POL, GND, CLR, SHDN>::intHndl()
{
  static boolean polarity;

  // Determine delay since last interrupt (for mA calculation)
  // Note that first interrupt will be incorrect (no previous time!)
  lasttime = time;
  time = micros();

  // Get polarity value
  polarity = digitalRead(POL);
  if (polarity) // high = charging
  {
    battery_mAh += ah_quanta;
  }
  else // low = discharging
  {
    battery_mAh -= ah_quanta;
  }

  // Calculate mA from time delay (optional)
  miliA = 614.4/((time-lasttime)/1000000.0);

  // If charging, we'll set mA negative (optional)
  if (polarity) miliA = miliA * -1.0;

  // Set isrflag so main loop knows an interrupt occurred
  isrflag = true;
  digitalWrite(LED_BUILTIN, HIGH);
}

#endif
