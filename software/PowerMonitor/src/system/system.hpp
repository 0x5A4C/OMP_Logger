#ifndef SYSTEM_HPP
#define SYSTEM_HPP

#include "powerMgmt.hpp"

#include <Arduino.h>
#include <Wire.h>
#include "lib/MicroLCD/MicroLCD.h"
#include "lib/SSD1306/SSD1306.h"
#include "lib/Ltc4150/Ltc4150.hpp"

namespace sys
{
  extern PowerMgmt powerMgmt;
  extern LCD_SSD1306 lcd;

  typedef Ltc4150<2, 3, 4, 5, 6, 7> pwrMon_t;
//  extern pwrMon_t powerMonitor;

  void init();
  void process();
}

#endif
