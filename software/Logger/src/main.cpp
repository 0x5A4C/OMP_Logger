#include "Arduino.h"

#include "app/app.hpp"
#include "platform/platform.hpp"
#include "system/system.hpp"

void setup()
{
  platform::init();
  sys::init();
  app::init();
}

void loop()
{
  platform::process();
  sys::process();
  app::process();
}
