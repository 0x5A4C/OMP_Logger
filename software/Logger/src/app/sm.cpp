#include "sm.hpp"

void Sm::init()
{
  Serial.begin(57600);
  while (!Serial)
  {
     ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("Sm init");  
}

void Sm::update()
{
  sfsm.update();
}
