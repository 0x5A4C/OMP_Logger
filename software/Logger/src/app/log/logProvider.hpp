#ifndef LOG_PROVIDER_HPP
#define LOG_PROVIDER_HPP

#include "Arduino.h"

class LogProvider
{
public:
  LogProvider()
  {
  };

  virtual void log(Stream* outStream) = 0;

protected:

private:

};

#endif
