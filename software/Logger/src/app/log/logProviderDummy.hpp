#ifndef LOG_PROVIDER_DUMMY_HPP
#define LOG_PROVIDER_DUMMY_HPP

#include "logProvider.hpp"

class LogProviderDummy: public LogProvider
{
public:
  LogProviderDummy()
  {

  };

  virtual void log(Stream* outStream)
  {
    outStream->print("Dummy logger");
  }

protected:
private:
};

#endif
