#ifndef LOGGERSD_HPP
#define LOGGERSD_HPP

#include "logger.hpp"
#include "logProviders.hpp"

#include <SD.h>
#include <SPI.h>

class LoggerSd: public Logger
{
public:
  LoggerSd(): inited(false)
  {};

  virtual void init()
  {
    inited = SD.begin();
  }

  virtual void begin()
  {
    logFile = SD.open("datalog.txt", FILE_WRITE);
    stream = &logFile;
  }

  virtual void end()
  {
    logFile.close();
  }

protected:
private:
  File logFile;
  bool inited;

};

#endif
