#ifndef LOG_PROVIDER_PRESTEMP_HPP
#define LOG_PROVIDER_PRESTEMP_HPP

#include "logProvider.hpp"
#include "system/systemComponents.hpp"

class LogProviderPresTemp: public LogProvider
{
public:
  LogProviderPresTemp(): ready(false)
  {
  };

  // virtual String getLog()
  // {
  //   String log = String("");
  //
  //   if(!ready)
  //   {
  //     log = String("Sensor not ready");
  //     ready = bmp085.begin();
  //   }
  //   else
  //   {
  //     log += String(bmp085.readPressure()) + String(" Pa");
  //     log += String(" ");
  //     log += String(bmp085.readTemperature()) + String(" *C");
  //   }
  //
  //   return log;
  // };

  // virtual char* getLog()
  // {
  //   clean();
  //
  //   if(!ready)
  //   {
  //     strcpy(logBuffer, "Sensor not ready");
  //     ready = bmp085.begin();
  //   }
  //   else
  //   {
  //     strcpy(logBuffer, ltoa(bmp085.readPressure(), logBuffer, 10));
  //     strcpy(logBuffer + 5, flot(logBuffer + 5, bmp085.readTemperature()));
  //   }
  //
  //   return logBuffer;
  // }

  virtual void log(Stream* outStream)
  {
      if(!ready)
      {
        outStream->print("Sensor not ready\n\r");
        ready = bmp085.begin();
      }
      else
      {
        outStream->print(clock.getYear());
        outStream->print('-');
        outStream->print(clock.getMonth());
        outStream->print('-');
        outStream->print(clock.getDay());

        outStream->print(' ');

        outStream->print(clock.getHour());
        outStream->print(':');
        outStream->print(clock.getMinute());
        outStream->print(':');
        outStream->print(clock.getSecond());

        outStream->print(' ');

        outStream->print(bmp085.readPressure());
        outStream->print(" Pa ");
        outStream->print(bmp085.readTemperature());
        outStream->print(" *C ");

        outStream->println(" ");
      }
  }

protected:
private:
  bool ready;

};

#endif
