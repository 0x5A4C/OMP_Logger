#ifndef LOGGER_UART_HPP
#define LOGGER_UART_HPP

#include "logger.hpp"

class LoggerUart: public Logger
{
public:
  LoggerUart(): inited(false)
  {};

  virtual void init()
  {
    Serial.begin(57600);
    while (!Serial)
    {
       ; // wait for serial port to connect. Needed for native USB port only
    }

    stream = &Serial;
  }

  virtual void begin()
  {

  }

  virtual void end()
  {

  }

protected:

private:
  bool inited;

};

#endif
