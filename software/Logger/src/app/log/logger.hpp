#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <stdint.h>

class Logger
{
public:
  Logger()
  {
  }

  virtual void init() = 0;
  virtual void begin() = 0;
  virtual void end() = 0;

  Stream* stream;

protected:

private:

};

#endif
