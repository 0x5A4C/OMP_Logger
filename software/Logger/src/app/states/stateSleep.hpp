#ifndef STATE_SLEEP_HPP
#define STATE_SLEEP_HPP

#include "stateCommon.hpp"
#include "stateWakeUp.hpp"

extern StateWakeUp stateWakeUp;

class StateSleep: public StateCommon
{
public:
  StateSleep(): StateCommon()
  {
  }

  virtual void init();
  virtual void enter();
  virtual void update();
  virtual void exit();

protected:
private:

};

#endif
