#ifndef STATE_INIT_HPP
#define STATE_INIT_HPP

#include "stateCommon.hpp"

class StateInit: public StateCommon
{
public:
  StateInit(): StateCommon()
  {
  }

  virtual void init();
  virtual void enter();
  virtual void update();
  virtual void exit();

protected:
private:
};

#endif
