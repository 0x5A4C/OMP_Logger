#include "stateLogTemp.hpp"
#include "app/sm.hpp"

void StateLogTemp::init()
{
  loggerSd.init();
  loggerUart.init();
}

void StateLogTemp::enter()
{
}

void StateLogTemp::update()
{
  setSyncProvider(RTC.get);

  loggerUart.begin();
  logProviderPresTemp.log(loggerUart.stream);
  loggerUart.end();

  loggerSd.begin();
  logProviderPresTemp.log(loggerSd.stream);
  loggerSd.end();

  ledBuildIn.on();
  delay(100);
  ledBuildIn.off();
  delay(100);

  this->sfsm->transitionTo(&(pSm->stateSleep));
}

void StateLogTemp::exit()
{
}
