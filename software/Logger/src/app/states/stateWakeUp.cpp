#include "stateWakeUp.hpp"
#include "app/sm.hpp"

void StateWakeUp::init()
{
}

void StateWakeUp::enter()
{
}

void StateWakeUp::update()
{
  this->sfsm->transitionTo(&(pSm->stateLogTemp));
}

void StateWakeUp::exit()
{
}
