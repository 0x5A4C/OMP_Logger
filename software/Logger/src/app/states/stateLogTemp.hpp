#ifndef STATE_LOG_TEMP_HPP
#define STATE_LOG_TEMP_HPP

#include "stateCommon.hpp"
#include "stateSleep.hpp"
#include "../log/loggerSd.hpp"
#include "../log/loggerUart.hpp"
#include "../log/logProviderPresTemp.hpp"

extern StateSleep stateSleep;

class StateLogTemp: public StateCommon
{
public:
  StateLogTemp(): StateCommon()
  {
  }

  virtual void init();
  virtual void enter();
  virtual void update();
  virtual void exit();

protected:
private:
  LoggerSd loggerSd;
  LoggerUart loggerUart;
  LogProviderPresTemp logProviderPresTemp;
};

#endif
