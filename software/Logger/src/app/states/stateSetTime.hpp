#ifndef STATE_SET_TIME_HPP
#define STATE_SET_TIME_HPP

#include "stateCommon.hpp"

class StateSetTime: public StateCommon
{
public:
  StateSetTime(): StateCommon()
  {
  }

  virtual void init();
  virtual void enter();
  virtual void update();
  virtual void exit();

protected:
private:
  time_t t;
  tmElements_t tm;
};

#endif
