#include "stateSleep.hpp"
#include "app/sm.hpp"


  void StateSleep::init()
  {
  }

  void StateSleep::enter()
  {
  }

  void StateSleep::update()
  {
    powerMgmt.gotoSleep();

    this->sfsm->transitionTo(&(pSm->stateWakeUp));
  }

  void StateSleep::exit()
  {
  }
