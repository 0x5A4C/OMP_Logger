#include "stateSetTime.hpp"
#include "app/sm.hpp"

void StateSetTime::init()
{

}

void StateSetTime::enter()
{
  setSyncProvider(RTC.get);
}

void StateSetTime::update()
{

  File setTimeFile;
  //2017-03-12 12:34:45
  char huourBuf[3];
  char minBuf[3];
  char secBuf[3];
  char yearBuf[5];
  char monthBuf[3];
  char dayBuf[3];

  if(SD.exists("setTime.txt"))
  {

    setTimeFile = SD.open("SETTIME.TXT", FILE_READ);
    if(setTimeFile)
    {
      setTimeFile.read(yearBuf, 4);
      yearBuf[4] = (char)0;
      setTimeFile.read();
      setTimeFile.read(monthBuf, 2);
      monthBuf[2] = (char)0;
      setTimeFile.read();
      setTimeFile.read(dayBuf, 2);
      dayBuf[2] = (char)0;

      setTimeFile.read();

      setTimeFile.read(huourBuf, 2);
      huourBuf[2] = (char)0;
      setTimeFile.read();
      setTimeFile.read(minBuf, 2);
      minBuf[2] = (char)0;
      setTimeFile.read();
      setTimeFile.read(secBuf, 2);
      secBuf[2] = (char)0;

      tm.Year = CalendarYrToTm(atoi(yearBuf));
      tm.Month =  atoi(monthBuf);
      tm.Day = atoi(dayBuf);
      tm.Hour =  atoi(huourBuf);
      tm.Minute = atoi(minBuf);
      tm.Second = atoi(secBuf);

      t = makeTime(tm);
      RTC.set(t);
      clock.setSysTime(t);

      setTimeFile.close();
      SD.remove("SETTIME.TXT");
    }
  }

  this->sfsm->transitionTo(&(pSm->stateLogTemp));
}

void StateSetTime::exit()
{
}
