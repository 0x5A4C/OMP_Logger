#ifndef STATE_WAKEUP_HPP
#define STATE_WAKEUP_HPP

#include "stateCommon.hpp"

class StateWakeUp: public StateCommon
{
public:
  StateWakeUp(): StateCommon()
  {
  }

  virtual void init();
  virtual void enter();
  virtual void update();
  virtual void exit();

protected:
private:

};

#endif
