#ifndef STATE_COMMON_HPP
#define STATE_COMMON_HPP

//#include "app/sm.hpp"
#include "../sfm/SFsm.hpp"
#include "system/systemComponents.hpp"

class Sm;

class StateCommon: public State
{
public:
  StateCommon(): State()
  {
  }

  virtual void init()
  {
  }

  virtual void enter()
  {
  }

  virtual void update()
  {
  }

  virtual void exit()
  {
  }

  Sm *pSm;

protected:
private:
};

#endif
