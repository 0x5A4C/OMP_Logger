#include "app.hpp"

#include "sm.hpp"

Sm sm;

void app::init(void)
{
  sm.init();
}

void app::process(void)
{
  sm.update();
}
