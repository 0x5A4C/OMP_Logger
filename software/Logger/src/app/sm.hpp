#ifndef SM_HPP
#define SM_HPP

#include "sfm/SFsm.hpp"
#include "states/stateInit.hpp"
#include "states/stateSetTime.hpp"
#include "states/stateLogTemp.hpp"
#include "states/stateSleep.hpp"
#include "states/stateWakeUp.hpp"

class Sm
{
public:
  Sm(): sfsm(&stateInit)
  {
    stateInit.sfsm = &sfsm;
    stateSetTime.sfsm = &sfsm;
    stateLogTemp.sfsm = &sfsm;
    stateSleep.sfsm = &sfsm;
    stateWakeUp.sfsm = &sfsm;

    stateInit.pSm = this;
    stateSetTime.pSm = this;
    stateLogTemp.pSm = this;
    stateSleep.pSm = this;
    stateWakeUp.pSm = this;
  }

  void init();
  void update();

  StateInit stateInit;
  StateSetTime stateSetTime;
  StateLogTemp stateLogTemp;
  StateSleep stateSleep;
  StateWakeUp stateWakeUp;

  SFsm sfsm;

protected:
private:
};

#endif
