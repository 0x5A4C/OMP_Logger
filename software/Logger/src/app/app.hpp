#ifndef APP_HPP
#define APP_HPP

namespace app
{
  void init();
  void process();
}

#endif
