
#ifndef SFSM_HPP
#define SFSM_HPP

class SFsm;

class State
   {
public:

   State()
      {
      };

   virtual ~State()
      {
      };

   virtual void init()
      {
      }
   virtual void enter() = 0;
   virtual void update() = 0;
   virtual void exit() = 0;

   SFsm* sfsm;
protected:
private:
   };

class SFsm
   {
public:

   enum MachineStep
      {
      eMachineStep_unknown = 0,
      eMachineStep_changeState,
      eMachineStep_initState,
      eMachineStep_enterState,
      eMachineStep_updateState,
      eMachineStep_exitState
         };

   SFsm(State* current)
      {
      lastState = currentState = nextState = current;
      machineStep = eMachineStep_initState;
      }

   SFsm& update()
      {
      switch (machineStep)
         {
         case eMachineStep_unknown:
            break;
         case eMachineStep_changeState:
            lastState = currentState;
            currentState = nextState;
            machineStep = eMachineStep_initState;
            break;
         case eMachineStep_initState:
            currentState->init();
            machineStep = eMachineStep_enterState;
            break;
         case eMachineStep_enterState:
            currentState->enter();
            machineStep = eMachineStep_updateState;
            break;
         case eMachineStep_updateState:
            currentState->update();
            if (currentState != nextState)
               {
               machineStep = eMachineStep_exitState;
               }
            break;
         case eMachineStep_exitState:
            currentState->exit();
            machineStep = eMachineStep_changeState;
            break;
         default:
            break;
         }

      return *this;
      }

   SFsm& transitionTo(State* state)
      {
      nextState = state;
      return *this;
      }

   State* getCurrentState() const
      {
      return currentState;
      }

   bool isInState(State &state) const
      {
      return (&state == currentState);
      }

   State* getLastState()const
      {
      return lastState;
      }

private:
   MachineStep machineStep;
   bool needToTriggerEnter;
   State* lastState;
   State* currentState;
   State* nextState;
   };



#endif /* SFSM_HPP */
