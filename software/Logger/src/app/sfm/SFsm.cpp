
#include "SFsm.hpp"
/*
State::State(void (*updateFunction)())
   {
   userEnter = 0;
   userUpdate = updateFunction;
   userExit = 0;
   }

State::State(void (*enterFunction)(), void (*updateFunction)(), void (*exitFunction)())
   {
   userEnter = enterFunction;
   userUpdate = updateFunction;
   userExit = exitFunction;
   }

void State::enter()
   {
   if (userEnter)
      {
      userEnter();
      }
   }

void State::update()
   {
   if (userUpdate)
      {
      userUpdate();
      }
   }

void State::exit()
   {
   if (userExit)
      {
      userExit();
      }
   }

 */

/*
SFsm::SFsm(State* current)
   {
   needToTriggerEnter = true;
   currentState = nextState = current;
   }

SFsm& SFsm::update()
   {
   //simulate a transition to the first state
   //this only happens the first time update is called
   if (needToTriggerEnter)
      {
      currentState->enter();
      needToTriggerEnter = false;
      }
   else
      {
      if (currentState != nextState)
         {
         immediateTransitionTo(nextState);
         }
      currentState->update();
      }
   return *this;
   }

SFsm& SFsm::transitionTo(State* state)
   {
   nextState = state;
   return *this;
   }

SFsm& SFsm::immediateTransitionTo(State* state)
   {
   currentState->exit();
   currentState = nextState = state;
   currentState->enter();
   return *this;
   }

State& SFsm::getCurrentState() const
   {
   return *currentState;
   }

bool SFsm::isInState(State &state) const
   {
   return (&state == currentState);
   }

 */