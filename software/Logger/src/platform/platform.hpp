#ifndef PLATFORM_HPP
#define PLATFORM_HPP

#include "lib/LowPower.h"

namespace platform
{
  void init();
  void process();
}

#endif
