#ifndef SYSTEM_HPP
#define SYSTEM_HPP

#include "systemComponents.hpp"

//#include "powerMgmt.hpp"
//#include "loggerSd.hpp"

#include <DS3232RTC.h>

namespace sys
{

//  extern PowerMgmt powerMgmt;
//  extern LoggerSd loggerSd;

  void init();
  void process();

  extern bool ready;
}

#endif
