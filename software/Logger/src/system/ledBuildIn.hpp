#ifndef LED_BUILD_IN_HPP
#define LED_BUILD_IN_HPP

#include "Arduino.h"

class LedBuildIn
{
public:
  LedBuildIn()
  {}

  void on()
  {
    digitalWrite(LED_BUILTIN, HIGH);
  }

  void off()
  {
    digitalWrite(LED_BUILTIN, LOW);
  }
};

#endif
