#include "system.hpp"

#include "Arduino.h"

//PowerMgmt sys::powerMgmt;
//LoggerSd sys::loggerSd;

// #include <SD.h>
// #include <SPI.h>

bool sys::ready = false;

void sys::init(void)
{
  pinMode(LED_BUILTIN, OUTPUT);

//  loggerSd.init();

  setSyncProvider(RTC.get);

  while(!sys::ready)
  {
    ledBuildIn.off();
    delay(100);
    ledBuildIn.on();
    delay(100);
    ledBuildIn.off();
    delay(300);
    ledBuildIn.on();
    delay(300);

    // sys::ready = SD.begin();
    sys::ready = true;
    delay(300);
  }
}

void sys::process(void)
{
//  powerMgmt.setOn();

  // digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  // delay(1000);                       // wait for a second
  // digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW

//  loggerSd.process();
}
