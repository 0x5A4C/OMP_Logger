#ifndef SYSTEM_COMPONENTS_HPP
#define SYSTEM_COMPONENTS_HPP

#include "Clock/ClockArduino.hpp"
#include "powerMgmt.hpp"
#include "ledBuildIn.hpp"

#include <SD.h>
#include <SPI.h>

#include <DS3232RTC.h>    //http://github.com/JChristensen/DS3232RTC
#include <Time.h>         //http://www.arduino.cc/playground/Code/Time
#include <Wire.h>         //http://arduino.cc/en/Reference/Wire (included with Arduino IDE)
#include "Comps/Adafruit_BMP085.h"

extern System::ClockArduino clock;
extern PowerMgmt powerMgmt;
extern LedBuildIn ledBuildIn;
//extern SDLib::SDClass SD;
extern Adafruit_BMP085 bmp085;

#endif
