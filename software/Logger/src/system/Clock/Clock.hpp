#ifndef CLOCK_H
#define CLOCK_H

#include "../Time/SystemTime.hpp"

namespace System
{
  class Clock
  {
  public:
    Clock() {};

    virtual systemTime_t getSysTime() = 0;
    virtual void setSysTime(systemTime_t timeToSet)  = 0;
    virtual void setSysTime(int year, int month, int day, int hour, int minute, int second)  = 0;

    virtual int getHour() = 0;
    virtual int getMinute() = 0;
    virtual int getSecond() = 0;
    virtual int getDay() = 0;
    virtual int getWeekday() = 0;
    virtual int getMonth() = 0;
    virtual int getYear() = 0;

  protected:
  private:
  };
}
#endif
