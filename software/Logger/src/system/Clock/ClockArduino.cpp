#include "ClockArduino.hpp"
#include "Arduino.h"

#include "Time.h"

System::ClockArduino::ClockArduino()
{
}

systemTime_t System::ClockArduino::getSysTime()
{
  return now();
}

void System::ClockArduino::setSysTime(systemTime_t timeToSet)
{
  setTime(timeToSet);
}

void System::ClockArduino::setSysTime(int year, int month, int day, int hour, int minute, int second)
{
  setTime(hour, minute, second ,day ,month, year);
}

int System::ClockArduino::getHour()
{
  return hour();
}

int System::ClockArduino::getMinute()
{
  return minute();
}

int System::ClockArduino::getSecond()
{
  return second();
}

int System::ClockArduino::getDay()
{
  return day();
}

int System::ClockArduino::getWeekday()
{
  return weekday();
}

int System::ClockArduino::getMonth()
{
  return month();
}

int System::ClockArduino::getYear()
{
  return year();
}
