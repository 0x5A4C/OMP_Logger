#ifndef CLOCK_ARDUINO_H
#define CLOCK_ARDUINO_H

#include "Clock.hpp"

namespace System
{
  class ClockArduino: public Clock
  {
  public:
     ClockArduino();

     virtual systemTime_t getSysTime();
     virtual void setSysTime(systemTime_t timeToSet);
     virtual void setSysTime(int year, int month, int day, int hour, int minute, int second);

     virtual int getHour();
     virtual int getMinute();
     virtual int getSecond();
     virtual int getDay();
     virtual int getWeekday();
     virtual int getMonth();
     virtual int getYear();
  protected:
  private:
  };
}
#endif
