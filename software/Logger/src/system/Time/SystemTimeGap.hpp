#ifndef SYS_TIME_GAP_H
#define SYS_TIME_GAP_H

class TimeGap
{
public:
   TimeGap(): gap(0)
   {};
   virtual bool passed(uint32_t gapToPass) = 0;
   virtual void reset() = 0;
protected:
   uint32_t gap;

private:
};

#endif
