#ifndef SYS_TIME_GAP_ARDUINO_H
#define SYS_TIME_GAP_ARDUINO_H

#include "stdint.h"
#include "Arduino.h"
#include "SystemTimeGap.hpp"

class TimeGapArduino: public TimeGap
{
public:
   TimeGapArduino(): TimeGap()
   {
     gap = millis();
   }

   bool passed(uint32_t gapToPass)
   {
      bool passed = false;

      if(millis() - gap >= gapToPass)
      {
         gap = millis();
         passed = true;
      }

      return passed;
   };

   virtual void reset()
   {
      gap = millis();
   };
protected:
private:
};

#endif
